module ChartMaker
  class << self
    LIGHT_GRAY = '#999'
    X_AXIS = { categories: (1..5).to_a,
               maxPadding: 0,
               lineColor: LIGHT_GRAY,
               tickColor: LIGHT_GRAY,
               tickmarkPlacement: 'on',
               title: {
                 text: 'Years',
                 style: {
                   color: LIGHT_GRAY
                 }
               },
               labels: {
                 y: 20,
                 style: {
                   color: LIGHT_GRAY
                 }
               }
             }
    Y_AXIS = { gridLineColor: '#fff',
               lineWidth: 1,
               tickWidth: 1,
               lineColor: LIGHT_GRAY,
               tickColor: LIGHT_GRAY,
               title:  {
                 text: 'Dollars',
                 style: {
                   color: LIGHT_GRAY
                 }
               },
               labels: {
                 style: {
                   color: LIGHT_GRAY
                 }
               }
             }

    def make_cost_chart(results)
      cost_without_bluefeet = results['typicalUnengagementCost']
      # cost_with_bluefeet = results['bluefeetUnengagementCost']

      divide_by_a_million!(cost_without_bluefeet)
      # divide_by_a_million!(cost_with_bluefeet)

      LazyHighCharts::HighChart.new do |f|
        f.title text: "Cash waste",
                margin: 0,
                style: {
                  fontSize: '10pt'
                }

        f.subtitle text: '(in million)',
                   align: 'left',
                   x: 0,
                   style: {
                     fontSize: '8pt',
                     color: LIGHT_GRAY
                   }

        f.chart backgroundColor: nil

        f.series type: 'column',
                 data: cost_without_bluefeet

        f.colors bar_chart_colors

        f.xAxis X_AXIS

        f.yAxis Y_AXIS

        f.legend enabled: false

        f.plotOptions series: {
                        animation: false
                      },
                      column: {
                        colorByPoint: true
                      }
      end
    end

    def make_benefits_chart(results)
      cost_of_bluefeet = results['bluefeetCost']
      bluefeet_net_value = results['bluefeetNetValue']
      bluefeet_cumulative_value = results['bluefeetCumulativeValue']

      divide_by_a_million!(cost_of_bluefeet)
      divide_by_a_million!(bluefeet_net_value)
      divide_by_a_million!(bluefeet_cumulative_value)

      LazyHighCharts::HighChart.new do |f|
        f.title text: "With Bluefeet",
                style: {
                  fontSize: '10pt'
                }

        f.series name: 'Net value (cash flow)',
                 data: bluefeet_net_value,
                 color: '#FECB2F'

        f.series name: 'Cumulative net value',
                 data: bluefeet_cumulative_value,
                 color: '#C3D63F'

        f.series name: 'Bluefeet cost',
                 data: cost_of_bluefeet,
                 color: '#5DC9E6'

        f.xAxis X_AXIS
        f.yAxis Y_AXIS

        f.legend symbolHeight: 13,
                 symbolWidth: 16,
                 layout: 'vertical',
                 x: -95,
                 y: 10,
                 floating: true,
                 itemStyle: {
                   color: LIGHT_GRAY,
                   paddingBottom: '3pt',
                   fontWeight: 100
                 }

        f.subtitle text: '(in million)',
                   align: 'left',
                   x: 0,
                   style: {
                     fontSize: '8pt',
                     color: LIGHT_GRAY
                   }

        f.plotOptions series: { animation: false,
                                marker: {
                                  fillColor: '#FFFFFF',
                                  lineWidth: 2,
                                  lineColor: nil,
                                  symbol: 'circle'
                                }
                              }
        f.chart marginBottom: 100,
                backgroundColor: nil
      end
    end

    def divide_by_a_million!(array)
      array.map!{ |num| num / 1_000_000.0 }
    end

    def bar_chart_colors
      opacity_values = (0.2..1).step(0.2).to_a
      opacity_values.map{ |num| "rgba(93, 201, 230, #{num})"}
    end
  end
end
