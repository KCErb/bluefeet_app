Rails.application.routes.draw do
  get 'calculator/' => 'calculator#start'
  get 'calculator/email-splash' => 'calculator#email_splash'
  get 'calculator/test-report' => 'report#report'

  post 'calculator/send-report' => 'calculator#send_report'

  get 'email-preview' => 'calculator#preview_email'
end
