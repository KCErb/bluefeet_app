# Configure the javascript tester: konacha
# Wrap in if to avoid call in production
if defined? Konacha
  Konacha.configure { |config| config.spec_dir = 'test/javascripts' }
end
