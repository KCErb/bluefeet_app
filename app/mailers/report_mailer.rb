require 'json'
require 'chart_maker'

class ReportMailer < ActionMailer::Base
  default from: "iamkcerb@gmail.com"
  layout 'mailer.html.haml'

  def report_email(params)
    return unless params['email'] =~ /(iamkcerb@gmail.com|warcher@lolofit.com)/
    @params = params
    @inputs = JSON.parse(params['inputs'])
    @results = JSON.parse(params['results'])
    @cost_chart = ChartMaker.make_cost_chart(@results)
    @benefits_chart = ChartMaker.make_benefits_chart(@results)

    @first_name = params['first_name']
    @first_name = 'there' if @first_name == ''
    @company_name = params['company_name']
    @company_name = 'your company' if @company_name == ''

    pdf = WickedPdf.new.pdf_from_string(
      render_to_string('report/report.pdf.haml', layout: 'pdf.haml'),
      margin:  { top: 0, bottom: 0, left: 0, right: 0 },
      disable_smart_shrinking: true,
      zoom: 0.75,
      layout: 'pdf.haml',
      page_size: 'Letter',
      javascript_delay: 2000
    )

    attachments.inline['bluefeet-report.pdf'] = pdf

    mail(to: params['email'], subject: 'Bluefeet Report').deliver
  end
end
