require 'chart_maker'

# Class used for previewing and testing the pdf
class ReportController < ApplicationController
  def report
    # Test Inputs and Results
    @params = params # make visible to view
    @inputs = test_inputs
    @results = test_results
    @cost_chart = ChartMaker.make_cost_chart(@results)
    @benefits_chart = ChartMaker.make_benefits_chart(@results)

    @company_name = 'your company'

    # Return a PDF
    respond_to do |format|
      # Only get pdf response if .pdf is added, so we'll add it
      format.html do
        redirect_to calculator_test_report_path + '.pdf?debug=1'
      end
      format.pdf do
        render pdf: 'bluefeet-report',
               margin:  { top: 0, bottom: 0, left: 0, right: 0 },
               # Shrinking and zoom work together. Shrinking auto-zooms which makes the layout unstable. Zoom is related to dpi.
               disable_smart_shrinking: true,
               zoom: 0.75,
               layout: 'pdf.haml',
               page_size: 'Letter',
               show_as_html: params[:debug],
               javascript_delay: 2000
      end
    end
  end

  def test_inputs
    {"employeeCount"    => {"min"=>0, "value"=>1000, "type"=>"number"},
     "teamGrowthRate"   => {"min"=>0, "value"=>10, "type"=>"percent"}, "engagementRate"   => {"min"=>0, "max"=>100, "step"=>1, "value"=>60, "type"=>"percent"},
     "turnoverRate"     => {"min"=>0, "max"=>100, "step"=>1, "value"=>15, "type"=>"percent"},
     "averageSalary"    => {"min"=>25, "value"=>100, "max"=>250, "step"=>5, "type"=>"k-dollars"},
     "salaryGrowthRate" => {"min"=>0, "max"=>10, "step"=>0.1, "value"=>3.8, "type"=>"percent"},
     "weightedAverageCostOfCapital" => {"min"=>0, "max"=>100, "step"=>1, "value"=>12, "type"=>"percent"}
    }
  end

  def test_results
    { "bluefeetCost"            => [1200000, 1320000, 1452000, 1597200, 1756800],
      "bluefeetNetValue"        => [-1200000, 165250, 1908768, 4154234, 6949665],
      "bluefeetCumulativeValue" => [-1200000, -1034750, 874018, 5028252, 11977917],
      "bluefeetNetPresentValue" => 7842742,
      "paybackMonths"           => 18.5,
      "internalReturnRate"      => 117,
      "bluefeetUnengagementCost" => [10725000, 12215500, 13921572, 15846045, 18079974],
      "typicalUnengagementCost"  => [10725000, 13700750, 17282341, 21597480, 26786438] }
  end

end
