require 'chart_maker'
class CalculatorController < ApplicationController
  def start
    @inputs = { employeeCount: 'How Many Employees Do You Have Today?',
                teamGrowthRate: 'How Fast Is Your Team Growing (% Per Year)?',
                engagementRate: 'How Engaged Is Your Team (% of Team)?',
                turnoverRate:   'What Is Your Annual Turn-Over Rate (% of Team)',
                averageSalary:  'What Is Your Average Employee Salary + Bonus ($K)?',
                salaryGrowthRate: 'How Fast Is The Average Employee Salary + Bonus Growing (% Per Year)?',
                weightedAverageCostOfCapital: "What Is Your Company's Weighted Average Cost of Capital (WACC)?"
              }
    @results = { bluefeetCost: 'Cost of Bluefeet, Company-wide',
                 bluefeetNetValue: 'Net Value to the Company (Cash Flow Impact)',
                 bluefeetCumulativeValue: 'Cumulative Value to the Company (Cumlative Cash Flow Impact)',
                 bluefeetNetPresentValue: 'Present Value of Bluefeet Implementation',
                 paybackMonths: 'Payback Period for Bluefeet Implementation',
                 internalReturnRate: 'Internal Rate of Return for Bluefeet Implementation',
               }
  end

  def send_report
    ReportMailer.delay.report_email(params)
    render nothing: true
  end

  def preview_email
    @first_name = 'there'
    @company_name = 'your company'
    render file: 'report_mailer/report_email.html.haml', layout: 'mailer.html.haml'
  end

  def email_splash
    @email = params['email']
  end
end
