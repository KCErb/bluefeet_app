class Calculator.Backend

  BLUEFEET_PRICE = 100        # Monthly price in dollars for Bluefeet per employee
  TYPICAL_ENGAGEMENT = 0.40   # Expected engagement after 5 years if nothing's done
  ENG_UNENG_RATIO = 0.30      # Ratio between Engaged & Unengaged turnover rates
  UNENG_ENG_RATIO = 1 - ENG_UNENG_RATIO
  REPLACEMENT_COST = 14500;   # Cost in dollars to replace a departed employee
  BLUEFEET_IMPROVEMENT = 0.05 # Expected engagement improvement due to bluefeet
  COST_OF_INEFFICIENCY = 0.25 # Portion of an employee's salary wasted when unengaged

  constructor: (@app) ->

  calculate: =>
    @getInputs(@app.inputs)
    @computeROE()

  getInputs: (inputObjects) =>
    @inputs = {}
    for key, object of inputObjects
      @inputs[key] = switch object.type
        when 'number' then object.value
        when 'percent' then object.value / 100
        when 'k-dollars' then object.value * 1000

  computeROE: =>
    @computeEmployeeCount()
    @computeSalary()
    @app.results.bluefeetCost = @computeBluefeetCost()
    @computeEngagementRates()
    @computeEngagedEmployeeCount()
    @computeUnengagedEmployeeCount()
    @computeDepartureProbability()
    @computeUnengagedDepartureCount()
    @computeKeptByBluefeet()
    @computeBluefeetGrossValue()
    @app.results.bluefeetNetValue = @computeBluefeetNetValue()
    @app.results.bluefeetCumulativeValue = @computeBluefeetCumulativeValue()
    @app.results.bluefeetNetPresentValue = @computeBluefeetNetPresentValue()
    @app.results.paybackMonths = @computePaybackMonths()
    @app.results.internalReturnRate = @computeInternalReturnRate()
    @app.results.typicalUnengagementCost = @computeTypicalUnengagementCost()
    @app.results.bluefeetUnengagementCost = @computeBluefeetUnengagementCost()

  # Initialize employee count array
  computeEmployeeCount: =>
    @employeeCount = []
    for yearNumber in [0..4]
      growth = (1 + @inputs.teamGrowthRate) ** yearNumber
      @employeeCount.push Math.floor(@inputs.employeeCount * growth)

  # Initialize salary array
  computeSalary: =>
    @salary = []
    for yearNumber in [0..4]
        growth = (1 + @inputs.salaryGrowthRate) ** yearNumber
        @salary.push @inputs.averageSalary * growth

  # Calculate the cost of Bluefeet each year
  computeBluefeetCost: =>
    @bluefeetCost = for count in @employeeCount
      Math.round(count * BLUEFEET_PRICE * 12)

  # Calculate the Engagement Rate, Over Time, Without and With Bluefeet
  computeEngagementRates: =>
    typicalRateOfDecline = (@inputs.engagementRate - TYPICAL_ENGAGEMENT) / 4
    bluefeetRateOfDecline = typicalRateOfDecline - BLUEFEET_IMPROVEMENT
    @bluefeetEngagementRate = for i in [0..4]
      rate = @inputs.engagementRate - i * bluefeetRateOfDecline
      rate = 1 if rate > 1 # These two lines are from the MATLAB code
      rate = 0 if rate < 0 # I don't think they're really necessary
      rate

    @typicalEngagementRate = for i in [0..4]
      @inputs.engagementRate - i * typicalRateOfDecline

  # Calculate the # Engaged Employees, Over Time, Without & With Bluefeet
  computeEngagedEmployeeCount: =>
    @bluefeetEngagedCount = []
    @typicalEngagedCount = []
    for i in [0..4]
      @bluefeetEngagedCount.push(
        Math.round(@employeeCount[i] * @bluefeetEngagementRate[i])
      )
      @typicalEngagedCount.push(
        Math.round(@employeeCount[i] * @typicalEngagementRate[i])
      )

  # Calculate the # Unengaged Employees, Over Time, Without & With Bluefeet
  computeUnengagedEmployeeCount: =>
    @bluefeetUnengagedCount = []
    @typicalUnengagedCount = []
    for i in [0..4]
      @bluefeetUnengagedCount.push @employeeCount[i] - @bluefeetEngagedCount[i]
      @typicalUnengagedCount.push @employeeCount[i] - @typicalEngagedCount[i]

  # Calculate the Probability of Departure for **Unengaged** Employees, Without & With Bluefeet
  computeDepartureProbability: =>
    @bluefeetDepartureProbability = []
    @typicalDepartureProbability = []
    for i in [0..4]
      departedEmployees = @inputs.turnoverRate * @employeeCount[i]
      bluefeetEngagedEmployeeTurnover =
        ENG_UNENG_RATIO * @bluefeetEngagedCount[i]
      typicalEngagedEmployeeTurnover =
        ENG_UNENG_RATIO * @typicalEngagedCount[i]
      @bluefeetDepartureProbability.push(
        departedEmployees /
        (bluefeetEngagedEmployeeTurnover + @bluefeetUnengagedCount[i])
      )
      @typicalDepartureProbability.push(
        departedEmployees /
        (typicalEngagedEmployeeTurnover + @typicalUnengagedCount[i])
      )

  # Calculate the Expected **Unengaged** Employee Departures, Without & With Bluefeet
  computeUnengagedDepartureCount: =>
    @bluefeetUnengagedDepartureCount = []
    @typicalUnengagedDepartureCount = []
    for i in [0..4]
      @bluefeetUnengagedDepartureCount.push(
        Math.floor(@bluefeetDepartureProbability[i] *
        @bluefeetUnengagedCount[i] *
        UNENG_ENG_RATIO)
      )

      @typicalUnengagedDepartureCount.push(
        Math.floor(@typicalDepartureProbability[i] *
        @typicalUnengagedCount[i] *
        UNENG_ENG_RATIO)
      )

  # Calculate the Number of Expected Avoided Departures, Due to Bluefeet
  computeKeptByBluefeet: =>
    @keptByBluefeet = []
    for i in [0..4]
      @keptByBluefeet.push(
        Math.round(@typicalUnengagedDepartureCount[i] * UNENG_ENG_RATIO) \
        - Math.round(@bluefeetUnengagedDepartureCount[i] * UNENG_ENG_RATIO)
      )

  computeBluefeetGrossValue: =>
    @bluefeetGrossValue = []
    for i in [0..4]
      numberOfNewlyengagedEmployees = @typicalUnengagedCount[i] - @bluefeetUnengagedCount[i]
      valueOfNewlyEngagedEmployees = numberOfNewlyengagedEmployees * COST_OF_INEFFICIENCY * @salary[i]
      @bluefeetGrossValue.push @keptByBluefeet[i] * REPLACEMENT_COST + valueOfNewlyEngagedEmployees

  computeBluefeetNetValue: =>
    @bluefeetNetValue = for i in [0..4]
      Math.round(@bluefeetGrossValue[i] - @bluefeetCost[i])

  computeBluefeetCumulativeValue: =>
    @bluefeetCumulativeValue = [@bluefeetNetValue[0]]
    for i in [1..4]
      @bluefeetCumulativeValue.push(
        @bluefeetCumulativeValue[i-1] + @bluefeetNetValue[i])
    @bluefeetCumulativeValue

  computeBluefeetNetPresentValue: =>
    @bluefeetNetPresentValue = 0
    for i in [0..4]
      @bluefeetNetPresentValue +=
        @bluefeetNetValue[i] / (1 + @inputs.weightedAverageCostOfCapital) ** i
    @bluefeetNetPresentValue = Math.round(@bluefeetNetPresentValue)

  computePaybackMonths: =>
    # Find index of first positive element in @bluefeetCumulativeValue
    positiveElements = @bluefeetCumulativeValue.filter (value) -> value >= 0
    firstPositiveElement = positiveElements[0]
    firstPositiveIndex = @bluefeetCumulativeValue.indexOf(firstPositiveElement)

    # Find Net Value element at first positive index
    netValue = @bluefeetNetValue[firstPositiveIndex]

    # Find last negative element of cumulative value
    lastNegativeIndex = firstPositiveIndex - 1
    lastNegativeElement = @bluefeetCumulativeValue[lastNegativeIndex]

    # Divide them to find fraction of switch year
    yearFraction = lastNegativeElement / netValue;

    # Add number of years until that switch took place
    yearsToSwitch = firstPositiveIndex - 1;
    paybackYears =  yearsToSwitch + Math.abs(yearFraction);

    # Convert to months
    @paybackMonths = paybackYears * 12
    @paybackMonths = Math.round(@paybackMonths * 10) / 10

  computeInternalReturnRate: =>
    # iteratively calculate the internal return rate
    @internalReturnRate = 0
    tolerance = 1
    maxIRR = 5
    step = 0.001
    # while loop for iterative search
    keepGoing = true
    while keepGoing
      @internalReturnRate += step
      # stop search if we find irrSum within tolerance
      keepGoing = false if @irrSum(@internalReturnRate) < tolerance
      # if we go over the max, increase the tolerance and start again
      if @internalReturnRate > maxIRR
        tolerance *= 2
        @internalReturnRate = 0 unless tolerance > 255
      # If we've already tried this 8 times then just give up.
      if tolerance > 255
        keepGoing = false
    @internalReturnRate = Math.round(@internalReturnRate * 100)

  irrSum: (irr) ->
    sum = Math.abs(@bluefeetNetValue[0] +
                   @bluefeetNetValue[1]/(1+irr) +
                   @bluefeetNetValue[2]/(1+irr)**2 +
                   @bluefeetNetValue[3]/(1+irr)**3 +
                   @bluefeetNetValue[4]/(1+irr)**4)
    sum /= 1000
  computeTypicalUnengagementCost: ->
    for i in [0..4]
      # Departures Due To Lack of Engagement, Regretted.
      departuresDueToUnengagement = Math.round(@typicalUnengagedDepartureCount[i] * UNENG_ENG_RATIO)
      Math.round(@typicalUnengagedCount[i] * @salary[i] * COST_OF_INEFFICIENCY +
      departuresDueToUnengagement * REPLACEMENT_COST)

  computeBluefeetUnengagementCost: ->
    for i in [0..4]
      departuresDueToUnengagement = Math.round(@bluefeetUnengagedDepartureCount[i] * UNENG_ENG_RATIO)
      Math.round(@bluefeetUnengagedCount[i] * @salary[i] * COST_OF_INEFFICIENCY +
      departuresDueToUnengagement * REPLACEMENT_COST)
