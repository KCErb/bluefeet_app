# TODO: Attach Calculator and app to window? How to best scope?
window.Calculator = {}

class Calculator.App
  constructor: ->
    dom = $("#calculator-gui")
    @results = {}
    @initInputs()
    @gui = new Calculator.Gui(this)
    @backend = new Calculator.Backend(this)
    @chart = new Calculator.Chart(this)

  initInputs: ->
    @inputs = {}
    @inputs.employeeCount    = { min: 0, value: 1000, type: 'number' }
    @inputs.teamGrowthRate   = { min: 0, value: 10, type: 'percent'}
    @inputs.engagementRate   = { min: 0, max: 100, step: 1, value: 60, type: 'percent' }
    @inputs.turnoverRate     = { min: 0, max: 100, step: 1, value: 15, type: 'percent' }
    @inputs.averageSalary    = { min: 25, value: 100, max: 250, step: 5, type: 'k-dollars' }
    @inputs.salaryGrowthRate = { min: 0, max: 10, step: 0.1, value: 3.8, type: 'percent'}
    @inputs.weightedAverageCostOfCapital = { min: 0, max: 100, step: 1, value: 12, type: 'percent' }

  updateResults: ->
    @backend.calculate()
    @chart.draw()

$(document).ready =>
  window.app = new Calculator.App()
  app.gui.draw() # separate draw from init for tests
  app.chart.draw()
