class Calculator.Chart
  constructor: (@app) ->

  draw: ->
    $('#chart-div').highcharts
      title:
        text: 'Value of Bluefeet'
        x: -20
      xAxis:
        title: text: 'Years'
        categories: [1..5]
      yAxis:
        title: text: 'Dollars'
      tooltip: valuePrefix: '$'
      legend:
        layout: 'vertical'
        align: 'right'
        verticalAlign: 'middle'
        borderWidth: 0
      series: [
        {
          name: 'Cost of Bluefeet'
          data: @app.results['bluefeetCost']
        }
        {
          name: 'Net Value to the Company'
          data: @app.results['bluefeetNetValue']
        }
        {
          name: 'Cumulative Value to the Company'
          data: @app.results['bluefeetCumulativeValue']
        }
      ]
      plotOptions:
        series:
          animation: false
