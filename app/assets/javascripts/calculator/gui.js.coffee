class Calculator.Gui

  RETURN_KEY_CODE = 10
  ENTER_KEY_CODE = 13

  constructor: (@app) ->

  draw: ->
    for key, object of @app.inputs
      # Grab text fields and sliders from view and 'build' them: attach
      # callbacks etc to them.
      textFieldElement = $("##{key}-text")
      sliderElement = $("##{key}-slider")
      @buildTextField(textFieldElement, key, object)
      if object.max
        @buildSlider(sliderElement, key, object)
      else
        sliderElement.hide()

    @updateGUI()

  updateGUI: =>
    @app.updateResults()
    for key, value of @app.results
      resultString = @formatResult(key, value)
      $("##{key}").html(resultString)

  formatResult: (key, value) ->
    switch key
      when 'bluefeetCost', 'bluefeetCumulativeValue', 'bluefeetNetValue'
        @formatMultiYearResult(value)
      when 'bluefeetNetPresentValue' then @formatNPV(value)
      when 'paybackMonths' then @formatPaybackMonths(value)
      when 'internalReturnRate' then @formatIRR(value)

  formatMultiYearResult: (valueArray) ->
    string = for index in [0..4]
      num = Math.round(valueArray[index] / 100) / 10
      "Year#{index + 1}: $#{num}K</br>"

  formatNPV: (value) ->
    if value < 0
      'Less than 0'
    else
      "$#{ value / 1000 }K"

  formatPaybackMonths: (value) ->
    if isNaN value
      "More than 4 years"
    else
      "#{value} Months"

  formatIRR: (value) ->
    if value > 499 # backend irr max = 5
      'Less than 0'
    else
      "#{value}%"

  buildSlider: (sliderElement, key, object) ->
    sliderElement.slider({
      min: object.min,
      max: object.max,
      step: object.step
      value: object.value,
      slide: (event, ui) =>
        # Just update the text field, since all elements have a text
        # field it contains logic for redoing calculation
        textFieldElement = $("##{key}-text")
        stringValue = String(ui.value)
        @updateTextField textFieldElement, stringValue
    })
    $("##{key}-slide-min").html(object.min)
    $("##{key}-slide-max").html(object.max)

  buildTextField: (textField, key, object) ->
    @setInitialFieldValues(textField, object)
    @attachTextFieldCallbacks(textField, key, object)

  setInitialFieldValues: (textField, object) ->
    # App inits with standard float inputs and we use the gui logic
    # to convert that in two directions: the gui and the backend. The gui
    # gets a nice string like "10%" and the backend gets just 10
    stringInput = String(object.value)
    [floatValue, stringValue] = @sanitizeInput(stringInput, object)
    @app.inputs[textField[0].name].value = floatValue
    textField.val(stringValue)

  attachTextFieldCallbacks: (textField, key, object) ->
    textField.bind 'blur keyup', (event) =>
      return if @keyupIsNotEnter(event)
      target = event.currentTarget
      floatValue = @updateTextField $(target), target.value # pass target twice since slider shares this interface
      @synchronizeSlider(key, floatValue) if object.max

  keyupIsNotEnter: (event) ->
    event.type == 'keyup' and event.which != ENTER_KEY_CODE

  updateTextField: (element, inputValue) ->
    inputObject = @app.inputs[element[0].name]
    [floatValue, stringValue] = @sanitizeInput(inputValue, inputObject)
    inputObject.value = floatValue
    element.val(stringValue)
    @updateGUI()
    return floatValue # synchronizeSlider uses return value

  synchronizeSlider: (key, value) ->
    $("##{key}-slider").slider('value', value)

  sanitizeInput: (value, object) ->
    number = @extractNumber(value, object)
    type = object.type
    stringValue = switch type
      when 'percent' then "#{number}%"
      when 'number' then "#{number}"
      when 'k-dollars' then "$#{number}K"
    [number, stringValue]

  extractNumber: (value, object) ->
    number = value.match(/\d+\.?\d*/)[0]
    number = parseFloat(number)
    if number > object.max then number = object.max
    if number < object.min then number = object.min
    number
