initForm = ->
  $('form').submit (event) ->
    event.preventDefault()
    inputs = encodeURIComponent(JSON.stringify(app.inputs))
    results = encodeURIComponent(JSON.stringify(app.results))
    first_name = $('input#first_name').val()
    company_name = $('input#company_name').val()
    email = $('input#email').val()
    # naive email pattern
    emailPattern = /// ^ #begin of line
      ([\w.-]+)         #one or more letters, numbers, _ . or -
      @                 #followed by an @ sign
      ([\w.-]+)         #then one or more letters, numbers, _ . or -
      \.                #followed by a period
      ([a-zA-Z.]{2,6})  #followed by 2 to 6 letters or periods
      $ ///i            #end of line and ignore case


    if email == ''
      alert 'Please enter an email.'
      return

    unless email.match( emailPattern )
      alert 'Please enter a valid email: foo@bar.com'
      return

    $.ajax
      url: "/calculator/send-report?email=#{email}&first_name=#{first_name}&company_name=#{company_name}&inputs=#{inputs}&results=#{results}",
      type: 'post',
      success: ->
        window.location.href = "/calculator/email-splash?email=#{email}"
      error: ->
        alert 'Error processing request'

$(document).ready ->
  initForm()
