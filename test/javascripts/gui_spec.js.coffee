#= require application
describe 'Calculator#Gui', ->
  before ->
    app = new Calculator.App
    @gui = app.gui

# Spec out methods other classes use, anything else in this file?
  it 'responds to draw', ->
    expect(@gui).to.respondTo('draw')
