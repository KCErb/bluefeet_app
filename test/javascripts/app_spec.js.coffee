#= require application
describe 'Calculator#App', ->
  beforeEach ->
    @app = new Calculator.App

  it 'has a gui', ->
    expect(@app.gui).to.be.an.instanceOf(Calculator.Gui)

  it 'has a backend', ->
    expect(@app.backend).to.be.an.instanceOf(Calculator.Backend)

  # TODO: Default tests?
  it 'uses correct input defaults to start', ->
    expect(@app.inputs.employeeCount.value).to.eql(1000)
