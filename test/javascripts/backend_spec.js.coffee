#= require application
describe 'Calculator#Backend', ->
  before ->
    @app = new Calculator.App
    @backend = @app.backend
    @backend.calculate()
    @results = @app.results

  it 'responds to calculate', ->
    expect(@backend).to.respondTo('calculate')

  it 'returns correct values for bluefeet cost', ->
    correctResults = [1200000, 1320000, 1452000, 1597200, 1756800]
    expect(@results.bluefeetCost).to.eql(correctResults)

  it 'returns correct values for net bluefeet value', ->
    correctResults = [-1200000, 165250, 1908768, 4154234, 6949665]
    expect(@results.bluefeetNetValue).to.eql(correctResults)

  it 'returns correct values for cumulative bluefeet value', ->
    correctResults = [-1200000, -1034750, 874018, 5028252, 11977917]
    expect(@results.bluefeetCumulativeValue).to.eql(correctResults)

  it 'returns correct value for net present value', ->
    correctResult = 7842742
    expect(@results.bluefeetNetPresentValue).to.eql(correctResult)

  it 'returns correct value for payback period in months', ->
    correctResult = 18.5
    expect(@results.paybackMonths).to.eql(correctResult)

  it 'returns correct value for internal return rate', ->
    correctResult = 117
    expect(@results.internalReturnRate).to.eql(correctResult)

  it 'returns correct values for typicalUnengagementCost', ->
    correctResult = [10725000, 13700750, 17282341, 21597480, 26786438]
    expect(@results.typicalUnengagementCost).to.eql(correctResult)

  it 'returns correct values for bluefeetUnengagementCost', ->
    correctResult = [10725000, 12215500, 13921572, 15846045, 18079974]
    expect(@results.bluefeetUnengagementCost).to.eql(correctResult)
