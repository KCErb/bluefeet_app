require 'test_helper'

class CalculatorControllerTest < ActionController::TestCase

  test "map 'root' to start action" do
    assert_routing('/calculator', controller: 'calculator', action: 'start')
  end

end
